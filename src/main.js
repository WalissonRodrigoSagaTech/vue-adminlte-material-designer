import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";

import Vuetify from "vuetify";
import "font-awesome/css/font-awesome.css";
import "./theme/default.styl";
import VeeValidate from "vee-validate";
import Truncate from "lodash.truncate";

Vue.config.productionTip = false;

// Helpers
// Global filters
Vue.filter("truncate", Truncate);
Vue.use(VeeValidate, { fieldsBagName: "formFields" });
Vue.use(Vuetify, {
  options: {
    themeVariations: ["primary", "secondary", "accent"],
    extra: {
      mainToolbar: {
        color: "primary"
      },
      sideToolbar: {},
      sideNav: "primary",
      mainNav: "primary lighten-1",
      bodyBg: ""
    }
  }
});

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
