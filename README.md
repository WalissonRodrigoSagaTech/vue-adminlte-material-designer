# Vue AdminLTE
Boiler Template in Vue/CLI 3.0 + Vue AdminLTE Material Theme.
Changes and refactor in this project is need to build a your version using this template.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
